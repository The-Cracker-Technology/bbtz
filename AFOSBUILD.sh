rm -rf /opt/ANDRAX/BBTz

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install wheel

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip3 install wheel... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/PYENV/python3/bin/pip3 install requests tldextract lxml jsbeautifier

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip3 install requirements... PASS!"
else
  # houston we have a problem
  exit 1
fi

go build -o andraxbin/aron aron.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build aron... PASS!"
else
  # houston we have a problem
  exit 1
fi

go build -o andraxbin/wbk wbk.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build wbk... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf *.py /opt/ANDRAX/PYENV/python3/bin/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
