#!/usr/bin/python

import requests
import json
import sys

if len(sys.argv) < 2 or len(sys.argv) > 4:
	print('%s [domain] [output]'%sys.argv[0])
	sys.exit(0)

all_links = []

domain = sys.argv[1]
try:
	filename = sys.argv[2]
except IndexError:
	filename = None

from urllib.parse import urlparse

parse = urlparse(domain)
if parse.netloc:
	domain = parse.netloc
elif parse.path != '' and parse.netloc == '':
	domain = parse.path
else: domain = domain

def archive():
	content = requests.get('http://web.archive.org/cdx/search/cdx?url=*.%s/*&output=json&collapse=urlkey'%domain).content
	c = json.loads(content)
	for i in c:
		for b in i:
			if domain in b and b.startswith('http'):
				if b not in all_links:
					b = b.replace('\u3000','')
					b = b.replace(r'\u','')
					all_links.append(b)
					print(b)
					if filename:
						file.write('%s\n'%b.encode('ascii', 'ignore').decode('ascii'))


##
## http://index.commoncrawl.org/CC-MAIN-2018-22-index?url=*.rezserver.com/*&output=json
##
if filename != None:
	file = open(filename,'w+')

archive()

if filename:
	file.close()
